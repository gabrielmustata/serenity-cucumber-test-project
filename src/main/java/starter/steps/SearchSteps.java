package starter.steps;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import starter.models.ErrorResponse;
import starter.models.Product;
import starter.models.ProductResponse;

import java.util.List;

public class SearchSteps {
    ObjectMapper objectMapper = new ObjectMapper();
    private ProductResponse productResponse;

    @SneakyThrows
    public ProductResponse searchProduct(String endpoint) {
        RestAssured.baseURI = "https://waarkoop-server.herokuapp.com";

        Response response = RestAssured
                .given()
                .when()
                .get(endpoint)
                .then()
                .log().all()
                .extract().response();

        productResponse = new ProductResponse();
        productResponse.setStatusCode(response.getStatusCode());

        if (!response.getBody().asString().isEmpty()) {
            if (response.getStatusCode() == 404) {
                // Handle 404 response separately
                ErrorResponse errorResponse = objectMapper.readValue(response.getBody().asString(), ErrorResponse.class);
                productResponse.setError(errorResponse);
            } else if (response.getBody().asString().startsWith("[")) {
                TypeReference<List<Product>> typeReference = new TypeReference<>() {
                };
                List<Product> productList = objectMapper.readValue(response.getBody().asString(), typeReference);
                productResponse.setProductList(productList);
            } else {
                Product singleProduct = objectMapper.readValue(response.getBody().asString(), Product.class);
                productResponse.setSingleProduct(singleProduct);
            }
        } else if (response.getStatusCode() == 404) {
            // Handle empty body for 404 response
            ErrorResponse errorResponse = ErrorResponse.createError(endpoint.substring(endpoint.lastIndexOf("/") + 1));
            productResponse.setError(errorResponse);
        }

        return productResponse;
    }


    public void validateStatusCode(ProductResponse productResponse, Integer expectedStatusCode) {
        int actualStatusCode = productResponse.getStatusCode();

        if (actualStatusCode == expectedStatusCode) {
            System.out.println("Status code is as expected: " + expectedStatusCode);
        } else {
            throw new AssertionError("Expected status code: " + expectedStatusCode +
                    ", but got: " + actualStatusCode);
        }
    }

    public void validateProductDetails(ProductResponse productResponse) {
        List<Product> products = productResponse.getProductList();

        if (products != null && !products.isEmpty()) {
            for (Product product : products) {
                validateProductTitle(product.getTitle());
            }
        } else if (productResponse.getSingleProduct() != null) {
            Product singleProduct = productResponse.getSingleProduct();
            validateProductTitle(singleProduct.getTitle());
        }
    }

    private void validateProductTitle(String title) {
        if (title != null && !title.isEmpty()) {
            System.out.println("Product title is valid: " + title);
        } else {
            System.out.println("Product title is empty or null");
        }
    }

    @SneakyThrows
    public void validateErrorMessage(String expectedErrorMessage) {
        int statusCode = productResponse.getStatusCode();

        if (statusCode == 404) {
            String actualErrorMessage = productResponse.getError().getDetail().getMessage();

            if (actualErrorMessage.equals(expectedErrorMessage)) {
                System.out.println("Error message is as expected: " + expectedErrorMessage);
            } else {
                throw new AssertionError("Expected error message: " + expectedErrorMessage +
                        ", but got: " + actualErrorMessage);
            }
        } else if (statusCode == 200) {
            // Status code is 200, no need to validate error message
            System.out.println("Status code is as expected: " + statusCode);
        } else {
            throw new AssertionError("Validation for error message is not applicable for status code: " + statusCode);
        }
    }

}
