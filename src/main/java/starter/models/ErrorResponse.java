package starter.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ErrorResponse {
    private Detail detail;

    @Data
    public static class Detail {
        private boolean error;
        private String message;
        private String requested_item;
        private String served_by;
    }

    public static ErrorResponse createError(String requestedItem) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setDetail(new Detail());
        errorResponse.getDetail().setError(true);
        errorResponse.getDetail().setMessage("Not found");
        errorResponse.getDetail().setRequested_item(requestedItem);
        errorResponse.getDetail().setServed_by("https://waarkoop.com");
        return errorResponse;
    }
}
