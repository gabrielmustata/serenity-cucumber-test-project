package starter.stepdefinitions;

import io.cucumber.java.en.*;
import net.serenitybdd.annotations.Steps;
import starter.models.ProductResponse;
import starter.steps.SearchSteps;

public class SearchStepDefinitions {

    @Steps
    SearchSteps searchSteps;
    private ProductResponse productResponse;

    @When("User calls endpoint {string}")
    public void user_calls_endpoint(String endpoint) {
        productResponse = searchSteps.searchProduct(endpoint);
    }

    @Then("he sees the status code {int}")
    public void he_sees_the_status_code(Integer expectedStatusCode) {
        searchSteps.validateStatusCode(productResponse, expectedStatusCode);
    }

    @Then("he sees the product details if the status code is 200")
    public void he_sees_the_product_details_if_status_code_is_200() {
        searchSteps.validateProductDetails(productResponse);
    }

    @Then("he sees an error message {string} if the status code is 404")
    public void he_sees_an_error_message_if_status_code_is_404(String expectedErrorMessage) {
        searchSteps.validateErrorMessage(expectedErrorMessage);
    }
}