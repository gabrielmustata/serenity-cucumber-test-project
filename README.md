# Serenity and Cucumber project

This is a test project using Serenity BDD and Cucumber.

Initially, I developed this project using Serenity BDD and Cucumber, 
assuming it was for a web application, specifically a car app. 
Later, I discovered additional requirements in a PDF document and the lease-example project.

Realizing the broader scope, I adjusted the project to meet these new requirements, 
resulting in the creation of multiple classes to cover various aspects. 
Despite the initial focus on a web app, the project successfully addressed the extended requirements outlined in the PDF.

# How to run the tests
1. Run the CucumberTestSuite class
2. Run from terminal `mvn clean verify`
3. Run from gitlab Build > Pipelines > Run pipeline

# Report
After the local run the report will be at target/site/serenity/index.html
In gitlab the report will be in Build > Artifacts

