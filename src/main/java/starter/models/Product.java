package starter.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Product {
    private String provider;
    private String title;
    private String url;
    private String brand;
    private Double price;
    private String unit;
    private boolean isPromo;
    private String promoDetails;
    private String image;
    private ErrorResponse.Detail errorDetail;

    public boolean isPromoDetails() {
        return "true".equalsIgnoreCase(promoDetails);
    }
}
