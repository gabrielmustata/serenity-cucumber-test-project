Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

#  Scenario:
#    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/orange"
#    Then he sees the results displayed for apple
#    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/apple"
#    Then he sees the results displayed for mango
#    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/car"
#    Then he doesn not see the results

  Scenario Outline: Search products by name
    When User calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/<product>"
    Then he sees the status code <expectedStatusCode>
    And he sees the product details if the status code is 200
    And he sees an error message "<expectedErrorMessage>" if the status code is 404

    Examples:
      | product | expectedStatusCode | expectedErrorMessage |
      | orange  | 200                |                      |
      | apple   | 200                |                      |
      | pasta   | 200                |                      |
      | cola    | 200                |                      |
      | car     | 404                | Not found            |
