package starter.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ProductResponse {

    private List<Product> productList;
    private Product singleProduct;
    private int statusCode;
    private ErrorResponse error;
}
